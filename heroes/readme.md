## Python致敬最美逆行者

相关视频：

[一段Python代码，致敬最美逆行者-哔哩哔哩-bilibili](https://www.bilibili.com/video/BV1qq4y1e7jr)


代码可下载、可修改、可商用，但需保留来源署名：**Crossin的编程教室**

![](效果图.png)

所用工具：Python3 及自带 turtle 库

更多实用有趣的例程

欢迎关注“**Crossin的编程教室**”公众号及同名 [B站](https://space.bilibili.com/17095888)

![crossincode](../crossin-logo.png)
