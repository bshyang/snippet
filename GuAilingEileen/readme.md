## Python画谷爱凌

代码可下载、可修改、可商用，但需保留来源署名：**Crossin的编程教室**

![](gal.png)

所用工具：Python3 及自带 turtle 库

更多实用有趣的例程

欢迎关注“**Crossin的编程教室**”及同名 [知乎专栏](https://zhuanlan.zhihu.com/crossin)

![crossincode](../crossin-logo.png)
